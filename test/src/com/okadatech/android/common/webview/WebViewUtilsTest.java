package com.okadatech.android.common.webview;

import android.content.Context;
import android.test.UiThreadTest;
import android.webkit.WebView;

import com.okadatech.android.common.test.InstrumentationTestCaseEx;

public class WebViewUtilsTest extends InstrumentationTestCaseEx {
    
    private Context mContext;

    protected void setUp() throws Exception {
        super.setUp();
        
        mContext = getTargetContext();
    }

    protected void tearDown() throws Exception {
        super.tearDown();
    }

    @UiThreadTest
    public void testDestroy() {
        final WebView webView = new WebView(mContext);
        WebViewUtils.destroy(webView);
        WebViewUtils.destroy(null);
    }

}
