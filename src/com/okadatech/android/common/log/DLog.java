package com.okadatech.android.common.log;

import java.util.Formatter;

import android.util.Log;
import com.okadatech.android.common.BuildConfig;

public final class DLog {

    public static final int DEBUG = Log.DEBUG;
    public static final int ERROR = Log.ERROR;
    public static final int INFO = Log.INFO;
    public static final int VERBOSE = Log.VERBOSE;
    public static final int WARN = Log.WARN;
    
    private DLog() { }

    private static boolean isLoggable() {
        return BuildConfig.DEBUG;
    }

    private static void log(int priority, String tag, String msg) {
        Throwable throwable = new Throwable();
        StackTraceElement[] stes = throwable.getStackTrace();
        StackTraceElement ste = stes[2];
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        sb.append(ste.getFileName());
        sb.append(":");
        sb.append(ste.getLineNumber());
        sb.append("]");
        sb.append(msg);
        Log.println(priority, tag, sb.toString());
    }

    private static void log(int priority, String tag, String format, Object... args) {
        Throwable throwable = new Throwable();
        StackTraceElement[] stes = throwable.getStackTrace();
        StackTraceElement ste = stes[2];
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        sb.append(ste.getFileName());
        sb.append(":");
        sb.append(ste.getLineNumber());
        sb.append("]");

        Formatter formatter = new Formatter(sb);
        formatter.format(format, args);

        Log.println(priority, tag, sb.toString());
        formatter.close();
    }

    public static void println(int priority, String tag, String msg) {

    }

    public static void critical(String tag, String msg) {
        log(ERROR, tag, msg);
    }

    public static void critical(String tag, String format, Object... args) {
        log(ERROR, tag, format, args);
    }

    public static void d(String tag, String msg) {
        if (!isLoggable()) {
            return;
        }

        log(DEBUG, tag, msg);
    }

    public static void d(String tag, String format, Object... args) {
        if (!isLoggable()) {
            return;
        }
        
        log(DEBUG, tag, format, args);
    }

    public static void e(String tag, String msg) {
        if (!isLoggable()) {
            return;
        }

        log(ERROR, tag, msg);
    }

    public static void e(String tag, String format, Object... args) {
        if (!isLoggable()) {
            return;
        }

        log(ERROR, tag, format, args);
    }

    public static void i(String tag, String msg) {
        if (!isLoggable()) {
            return;
        }

        log(INFO, tag, msg);
    }

    public static void i(String tag, String format, Object... args) {
        if (!isLoggable()) {
            return;
        }

        log(INFO, tag, format, args);
    }
    
    public static void v(String tag, String msg) {
        if (!isLoggable()) {
            return;
        }

        log(VERBOSE, tag, msg);
    }

    public static void v(String tag, String format, Object... args) {
        if (!isLoggable()) {
            return;
        }

        log(VERBOSE, tag, format, args);
    }

    public static void w(String tag, String msg) {
        if (!isLoggable()) {
            return;
        }

        log(WARN, tag, msg);
    }

    public static void w(String tag, String format, Object... args) {
        if (!isLoggable()) {
            return;
        }

        log(WARN, tag, format, args);
    }    
}
