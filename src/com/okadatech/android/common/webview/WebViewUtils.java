package com.okadatech.android.common.webview;

import android.webkit.WebView;

public final class WebViewUtils {

    private WebViewUtils() { }
    
    public static void destroy(final WebView webView) {
        if (webView == null) {
            return;
        }
        
        webView.clearView();
        webView.stopLoading();
        webView.setWebChromeClient(null);
        webView.setWebViewClient(null);
        webView.clearCache(false);
        webView.destroyDrawingCache();
        webView.destroy();
    }
}
